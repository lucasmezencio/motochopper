# Motochopper

Motochopper: Android root exploit

Copyright (C) 2013 Dan Rosenberg (@djrbliss)

Linux and OSX versions by Lucas Mezêncio (@lucasmezencio)

## Instructions

### Windows

1. Ensure you have the latest drivers installed from your device manufacturer.

2. Put your device in debugging mode

3. Attach it via USB

4. Run run.bat script in the same directory as the rest of the files in repository

### OSX

1. Put your device in debugging mode

2. Attach it via USB

3. Run run.osx.sh script in the same directory as the rest of the files in repository

### Linux

1. Put your device in debugging mode

2. Attach it via USB

3. Run run.linux.sh script in the same directory as the rest of the files in repository