#!/bin/bash

# ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# ::
# ::  Motochopper: Android root exploit
# ::  Windows version
# ::
# ::  Copyright (C) 2013 Dan Rosenberg (@djrbliss)
# ::
# ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# ::
# :: Instructions:
# ::
# ::  1. Put your device in debugging mode
# ::
# ::  2. Attach it via USB
# ::
# ::  3. Run this script in the same directory as the rest of the files in repository
# ::
# ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

echo "[*]"
echo "[*] Motochopper: Android root exploit (OSX version)"
echo "[*] v1.0"
echo "[*] by Lucas Mezêncio (@lucasmezencio)"
echo "[*]"
echo "[*] Tested on the LG Optimus G (E977, E975), Motorola Razr HD, Razr M, Razr Maxx HD, and Atrix HD."
echo "[*] Supports lots of other devices as well. ;-)"
echo "[*]"
echo "[*] Before continuing, ensure that USB debugging is enabled"
echo "[*] and that your phone is connected via USB."
echo "[*]"
echo "[*] WARNING: This will likely void the warranty on your device."
echo "[*] I am not responsible for any damage to your phone as a result using this tool."
echo "[*]"

read -p  "[*] Press enter to root your phone..."

echo "[*]"

./adb.osx kill-server

echo "[*] Waiting for device..."

./adb.osx wait-for-device

echo "[*] Device found."
echo "[*] Pushing exploit..."

./adb.osx push pwn /data/local/tmp/pwn
./adb.osx shell chmod 755 /data/local/tmp/pwn

echo "[*] Pushing root tools..."

./adb.osx push su /data/local/tmp/su
./adb.osx push busybox /data/local/tmp/busybox
./adb.osx install Superuser.apk

echo "[*] Rooting phone..."

./adb.osx shell /data/local/tmp/pwn

echo "[*] Cleaning up..."

./adb.osx shell rm /data/local/tmp/pwn
./adb.osx shell rm /data/local/tmp/su
./adb.osx shell rm /data/local/tmp/busybox

read -p "[*] Exploit complete. Press enter to reboot and exit."

./adb.osx reboot
./adb.osx kill-server